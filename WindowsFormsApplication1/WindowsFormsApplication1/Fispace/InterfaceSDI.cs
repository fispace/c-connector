﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using FispaceClientApplication.Fispace.Api;

namespace FispaceClientApplication.Fispace
{
    interface InterfaceSDI
    {
        string getCapabilities();
        string getCapability(int id);
        string addCapability(Capability capability);
        string setCapability(Capability capability);
        string removeCapability(int id);
        string getCapabilityTypes();
        string getCapabilityType(int id);
        string addCapabilityType(CapabilityType capabilityType);
        string setCapabilityType(CapabilityType capabilityType);
        string removeCapabilityType(int id);
        string getBusinessProcessTemplates();
        string getBusinessProcessTemplate(int id);
        string addBusinessProcessTemplate(BusinessProcessTemplate businessProcessTemplate);
        string setBusinessProcessTemplate(BusinessProcessTemplate businessProcessTemplate);
        string removeBusinessProcessTemplate(int id);
        string getBusinessProcesses();
        string getBusinessProcess(int id);
        string addBusinessProcess(BusinessProcess businessProcess);
        string setBusinessProcess(BusinessProcess businessProcess);
        string removeBusinessProcess(int id);

        string useCapability(Capability capability, string requestMessage);
    }
}
