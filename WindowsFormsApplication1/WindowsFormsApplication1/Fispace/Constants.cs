﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FispaceClientApplication.Fispace
{
    public class Constants
    {
        // private string url = "http://37.131.251.57:8080/sdi/admin-api/";

        private string url = "https://sdi-pie.fispace.eu:8443/sdi/admin-api/";
        private string url_use = "https://sdi-pie.fispace.eu:8443/sdi/api/";

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        public string Url_Use
        {
            get { return url_use; }
            set { url_use = value; }
        }
    }
}