﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FispaceClientApplication.Fispace.Api
{
    class CapabilityType
    {
        private int id;

        protected int Id
        {
            get { return id; }
            set { id = value; }
        }
        private String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        private String requestMessageType;

        public String RequestMessageType
        {
            get { return requestMessageType; }
            set { requestMessageType = value; }
        }
        private String responseMessageType;

        public String ResponseMessageType
        {
            get { return responseMessageType; }
            set { responseMessageType = value; }
        }
        private String schemaLocation;

        public String SchemaLocation
        {
            get { return schemaLocation; }
            set { schemaLocation = value; }
        }
        private String contextPath;

        public String ContextPath
        {
            get { return contextPath; }
            set { contextPath = value; }
        }

        public CapabilityType(string myName, string myRequestMessageType,
            string myResponseMessageType, string mySchemaLocation, string myContextPath)
        {
            name = myName;
            requestMessageType = myRequestMessageType;
            responseMessageType = myResponseMessageType;
            schemaLocation = mySchemaLocation;
            contextPath = myContextPath;
        }

    }
}
