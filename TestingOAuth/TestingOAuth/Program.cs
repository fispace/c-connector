﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

using System.Net.Http;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Net.Http.Headers;

using TestingOAuth.OAuth2.Providers;
using TestingOAuth.OAuth2;

using FispaceClientApplication.Fispace;
using FispaceClientApplication.Fispace.Api;


namespace TestingOAuth
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Console.WriteLine("Testing ...");
            Console.WriteLine();

            try
            {
                //Get first a valid token!
                SDIController mySDIController = new SDIController("eyJhbGciOiJSUzI1NiJ9.eyJuYW1lIjoiTSBWSiIsImVtYWlsIjoiUzI4NzUyNkBnbWFpbC5jb20iLCJqdGkiOiJjMDViYjU0NS1kNjYwLTRlYTgtOGE4Mi0wNGVhMjNjYTdjY2QiLCJleHAiOjE0NDIzMzc1NjIsIm5iZiI6MCwiaWF0IjoxNDQyMzM3NTAyLCJpc3MiOiJmaXNwYWNlIiwiYXVkIjoic2RpLW9hdXRoLWNsaWVudCIsInN1YiI6IjUxYTQ1Zjg0LTJhYzItNDlmMC05NTE3LTgzZGIwNmFhZTQ3NCIsImF6cCI6InNkaS1vYXV0aC1jbGllbnQiLCJnaXZlbl9uYW1lIjoiTSIsImZhbWlseV9uYW1lIjoiVkoiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJTMjg3NTI2IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJzZXNzaW9uX3N0YXRlIjoiOTgwMDlmMmYtNzA1ZS00OTg4LWE3ODYtOGExNmY1ZGZjODIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImFwcF9kZXZlbG9wZXIiLCJidXNpbmVzc19hcmNoaXRlY3QiXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50Iiwidmlldy1wcm9maWxlIl19fX0.Ro70__1S2y3vuhVcK1eg0gX_4DBvERdaeZ046qN9Dr7DCpGgAc9rB9GtpoSvXoJIcpbf1bI2YrMaCPaLJDUgpXoJyK8nhZQ-pk2NQLGBqIyWO3tBTQn_k3yYf68fzpNR_i6LMtgEhSxNRiN65PxP3TcqzAGSWjfjQP_3x6kRcwc"); // Your acces token


                /*******************************************/
                /* USE CAPABILITY
                /*******************************************/

                string myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                
                    "<ns3:ReceiveSensorValuesRequestMessage xmlns:ns2=\"http://www.limetri.eu/schemas/ygg\" xmlns:ns3=\"http://www.fispace.eu/domain/ag\">" +
                      "<ns2:businessProcessId>21</ns2:businessProcessId>" +
                      "<FarmID>1</FarmID>" +
                      "<CropFieldID>2</CropFieldID>" +
                      "<Greenhouse>" +
                        "<GreenhouseID>2</GreenhouseID>" +
                        "<GreenhouseDesignator>1</GreenhouseDesignator>" +
                      "</Greenhouse>" +
                      "<TimestampOfSensorMeasurement>2015-09-15T09:33:20.754+02:00</TimestampOfSensorMeasurement>" +
                      "<Temperature>68.04479942142638</Temperature>" +
                      "<Luminosity>4.4</Luminosity>" +
                      "<AirHumidity>70.735534108016</AirHumidity>" +
                      "<PH>6.6</PH>" +
                      "<EC>3.3</EC>" +
                      "<SoilMoisture>50</SoilMoisture>" +
                      "<CO2>1.1</CO2>" +
                      "<SoilTemperature>66.62965192193623</SoilTemperature>" +
                      "<PAR>5.5</PAR>" +
                    "</ns3:ReceiveSensorValuesRequestMessage>";

                Console.WriteLine("USE ");
                Console.Write("Using: " + mySDIController.useCapability(new Capability("myCapToUse", "http://localhost:8080", 18), myMessage));
                Console.WriteLine();


                /****
                ****/
                List<int> list3 = new List<int>();
                //list3.Add(6);
                //list3.Add(9);
                list3.Add(7);

                Console.WriteLine("ADD BP - httpClient");
                BusinessProcess myNewBp = new BusinessProcess("myBP", 3, list3);
                Console.Write("Creating: " + mySDIController.addBusinessProcess(
                    myNewBp));
                Console.WriteLine();

                List<int> list00 = new List<int>();
                list00.Add(7);

                List<int> list10 = new List<int>();
                list10.Add(11);
                list10.Add(13);

                List<int> list20 = new List<int>();
                list20.Add(12);
                list20.Add(14);


                List<BusinessProcess.BusinessProcessRole> bpRoleList =
                    new List<BusinessProcess.BusinessProcessRole>();
                bpRoleList.Add(new BusinessProcess.BusinessProcessRole("boxman", list00));
                bpRoleList.Add(new BusinessProcess.BusinessProcessRole("customer", list10));
                bpRoleList.Add(new BusinessProcess.BusinessProcessRole("supplier", list20));

                BusinessProcess myNewBp0 = new BusinessProcess("myBP0", 2, bpRoleList);
                Console.Write("Creating: " + mySDIController.addBusinessProcess(
                    myNewBp0));
                Console.WriteLine();
                

                Console.WriteLine();

                /*******************************************/
                /* BUSINESS PROCESS TEMPLATE
                /*******************************************/

               /*
                Console.WriteLine("DELETE BPT- httpClient");
                Console.Write("Deleting: " + mySDIController.removeBusinessProcessTemplate(9));
                Console.WriteLine();
                Console.WriteLine("DELETE BPT- httpClient");
                Console.Write("Deleting: " + mySDIController.removeBusinessProcessTemplate(8));
                Console.WriteLine();
                Console.WriteLine("DELETE BPT- httpClient");
                Console.Write("Deleting: " + mySDIController.removeBusinessProcessTemplate(7));
                Console.WriteLine();
                * */

                Console.WriteLine("BUSINESS PROCESS TEMPLATES");
                Console.Write("Getting: " + mySDIController.getBusinessProcessTemplates());
                Console.WriteLine();

                List<int> list = new List<int>();
                list.Add(2);
                list.Add(3);
                list.Add(1);


                List<int> list0 = new List<int>();
                list0.Add(2);

                List<int> list1 = new List<int>();
                list1.Add(3);
                list1.Add(1);
                
                List<BusinessProcessTemplate.BusinessProcessTemplateRole> bptRoleList = 
                    new List <BusinessProcessTemplate.BusinessProcessTemplateRole>();
                bptRoleList.Add(new BusinessProcessTemplate.BusinessProcessTemplateRole("SDK_role1", list));
                bptRoleList.Add(new BusinessProcessTemplate.BusinessProcessTemplateRole("SDK_role2", list0));

                Console.WriteLine("ADD BPT - httpClient");

                BusinessProcessTemplate mybpt0 = new BusinessProcessTemplate("myBPT_role", null, bptRoleList);
                BusinessProcessTemplate mybpt = new BusinessProcessTemplate("myBPT_role", bptRoleList);
                BusinessProcessTemplate mybpt1 = new BusinessProcessTemplate("myBPT_ct", list1);

                Console.Write("Creating myBPT_role: " + mySDIController.addBusinessProcessTemplate(
                    mybpt0));
                Console.WriteLine();
                Console.Write("Creating myBPT_role: " + mySDIController.addBusinessProcessTemplate(
                    mybpt));
                Console.WriteLine();
                Console.Write("Creating myBPT_ct: " + mySDIController.addBusinessProcessTemplate(
                    mybpt1));
                Console.WriteLine();

                /*Does not update. Adds a BPT cap with the same name. 
                List<int> list2 = new List<int>();
                list2.Add(2);
                list2.Add(3);
                
                Console.WriteLine("UPDATE BPT - httpClient");
                Console.Write("Updating: " + mySDIController.setBusinessProcessTemplate(
                    new BusinessProcessTemplate("myBPT1", list2)));
                Console.WriteLine();
                */

                Console.WriteLine("GET BPT - httpClient");
                Console.Write("Getting: " + mySDIController.getBusinessProcessTemplate(3));
                Console.WriteLine();

                Console.WriteLine("DELETE BPT- httpClient");
                Console.Write("Deleting: " + mySDIController.removeBusinessProcessTemplate(10));
                Console.WriteLine();






                Console.WriteLine("CAPABILITY TYPES - httpClient");
                Console.Write("Getting: " + mySDIController.getCapabilityTypes());
                Console.WriteLine();


                Console.WriteLine("BUSINESS PROCESS ");
                Console.Write("Getting: " + mySDIController.getBusinessProcesses());
                Console.WriteLine();

                //List<int> list3 = new List<int>();
                list3.Add(6);
                //list3.Add(9);
                list3.Add(7);

                /*
                List<int> list = new List<int>();
                
                list.Add(2);
                list.Add(3);
                list.Add(1);

                Console.WriteLine("ADD BPT - httpClient");
                BusinessProcessTemplate mybpt = new BusinessProcessTemplate("myBPT1", list);

                Console.Write("Creating: " + mySDIController.addBusinessProcessTemplate(mybpt));
                Console.WriteLine();
                */

                Console.WriteLine("ADD BP - httpClient");
                BusinessProcess mybp = new BusinessProcess("myBP2", 2, list3);

                Console.Write("Creating: " + mySDIController.addBusinessProcess(
                    mybp));
                Console.WriteLine();
                
                Console.WriteLine();
                


                

                /*******************************************/
                /* BUSINESS PROCESS
                /*******************************************/

                Console.WriteLine("BUSINESS PROCESS ");
                Console.Write("Getting: " + mySDIController.getBusinessProcesses());
                Console.WriteLine();

               /*Console.WriteLine("ADD CAPABILITY - httpClient");
                Console.Write("Creating: " + mySDIController.addCapability(new Capability("myCap1", "b", 29)));
                Console.WriteLine();
                 * */
                //List<int> list3 = new List<int>();
                list3.Add(6);
                //list3.Add(3);
                //list3.Add(1);

                /*
                List<int> list = new List<int>();
                
                list.Add(2);
                list.Add(3);
                list.Add(1);

                Console.WriteLine("ADD BPT - httpClient");
                BusinessProcessTemplate mybpt = new BusinessProcessTemplate("myBPT1", list);

                Console.Write("Creating: " + mySDIController.addBusinessProcessTemplate(mybpt));
                Console.WriteLine();
                */
                
                Console.WriteLine("ADD BP - httpClient");
                //BusinessProcess mybp = new BusinessProcess("myBP1", 10, list3);

                Console.Write("Creating: " + mySDIController.addBusinessProcess(
                    mybp));
                Console.WriteLine();

                /*Does not update 
                List<int> list4 = new List<int>();
                //list4.Add(2);
                //list4.Add(3);
                
                Console.WriteLine("UPDATE BP - httpClient");
                Console.Write("Updating: " + mySDIController.setBusinessProcess(
                    new BusinessProcess("myBP", 10, list4)));
                Console.WriteLine();
                */

                Console.WriteLine("GET BP - httpClient");
                Console.Write("Getting: " + mySDIController.getBusinessProcess(5));
                Console.WriteLine();

                Console.WriteLine("DELETE BP- httpClient");
                Console.Write("Deleting: " + mySDIController.removeBusinessProcess(5));
                Console.WriteLine();


                

                /*******************************************/
                /* CAPABILITIES
                /*******************************************/
              
                Console.WriteLine("CAPABILITIES");
                Console.Write("Getting: " + mySDIController.getCapabilities());
                Console.WriteLine();

                Console.WriteLine("ADD CAPABILITY - httpClient");
                Console.Write("Creating: " + mySDIController.addCapability(new Capability("myCap1", "b", 29)));
                Console.WriteLine();

                /* Does not update. Adds a new cap with the same name. 
                Console.WriteLine("UPDATE CAPABILITY - httpClient");
                Console.Write("Updating: " + mySDIController.setCapability(new Capability("myCap1", "c", 29)));
                Console.WriteLine();
                */

                Console.WriteLine("GET CAPABILITY - httpClient");
                Console.Write("Getting: " + mySDIController.getCapability(7));
                Console.WriteLine();

                Console.WriteLine("DELETE CAPABILITY- httpClient");
                Console.Write("Deleting: " + mySDIController.removeCapability(7));
                Console.WriteLine();


                /*******************************************/
                /* CAPABILITY TYPES
                /*******************************************/

                Console.WriteLine("CAPABILITY TYPES - httpClient");
                Console.Write("Getting: " + mySDIController.getCapabilityTypes());
                Console.WriteLine();

                Console.WriteLine("ADD CAPABILITY TYPE - httpClient");
                Console.Write("Creating: " + mySDIController.addCapabilityType(new CapabilityType("myCapTypeTEST22", "b", "c", "d", "e")));
                Console.WriteLine();

                /* Does not update. Does not add a new captype with the same name.
                Console.WriteLine("UPDATE CAPABILITY TYPE - httpClient");
                Console.Write("Updating: " + mySDIController.setCapabilityType(new CapabilityType("myCapTypeTEST22", "b", "c", "d", "e")));
                Console.WriteLine();
                */

                Console.WriteLine("GET CAPABILITY TYPE - httpClient");
                Console.Write("Getting: " + mySDIController.getCapabilityType(29));
                Console.WriteLine();

                Console.WriteLine("DELETE CAPABILITY TYPE - httpClient");
                Console.Write("Deleting: " + mySDIController.removeCapabilityType(36));
                Console.WriteLine();




            }
            catch (WebException e)
            {
                // This exception will be raised if the server didn't return 200 - OK
                // Retrieve more information about the error
                if (e.Response != null)
                {
                    using (HttpWebResponse err = (HttpWebResponse)e.Response)
                    {
                        Console.WriteLine("The server returned '{0}' with the status code '{1} ({2:d})'.",
                          err.StatusDescription, err.StatusCode, err.StatusCode);
                    }
                }
            }
            return;

        }
    }
}
