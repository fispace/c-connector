﻿/*
 *  Version 0.16.0-S1538
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;

using FispaceClientApplication.Fispace.Api;


namespace FispaceClientApplication.Fispace
{
    class SDIController : InterfaceSDI
    {
        Constants constants;
        //private string accessToken;

        public SDIController() { }

        public SDIController(string myAccessToken)
        {
            constants = new Constants();
            AccessToken = myAccessToken;
        }

        public string AccessToken { get; set; }

        public override string ToString()
        {
            return AccessToken;
        }

        //CAPABILITY MANAGEMENT ******************************************************

        //GET
        public string getCapabilities()
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);


                return client.GetAsync("capabilities")
                    .Result
                    .Content.ReadAsStringAsync().Result;
            }

        }

        //GET
        public string getCapability(int id)
        {
            string capability = String.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.GetAsync("capabilities/" + id);
                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    using (HttpContent content = res.Content)
                    {
                        capability = content.ReadAsStringAsync().Result;
                    }
                }
                else
                {
                    Console.Write("ERROR:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return capability;
        }

        //POST does not work. Use PUT instead.
        public string addCapability(Capability capability)
        {

            string newCapability = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<Capability xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<description>" + capability.Description + "</description>" +
                    "<uri>" + capability.Uri + "</uri>" +
                    "<cte_id>" + capability.CteId + "</cte_id>" +
                    "</Capability>";

                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                //Task<HttpResponseMessage> response = client.PostAsync("capabilities", capability);
                Task<HttpResponseMessage> response = client.PutAsync("capabilities", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newCapability = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR adding cap:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newCapability;
        }

        //PUT
        public string setCapability(Capability capability)
        {
            string newCapability = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<Capability xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<description>" + capability.Description + "</description>" +
                    "<uri>" + capability.Uri + "</uri>" +
                    "<cte_id>" + capability.CteId + "</cte_id>" +
                    "</Capability>";

                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                Task<HttpResponseMessage> response = client.PutAsync("capabilities", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newCapability = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR updating cap:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newCapability;
        }

        //DELETE
        public string removeCapability(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.DeleteAsync("capabilities/" + id);
                HttpResponseMessage res = response.Result;

                /*
                if (res.IsSuccessStatusCode)
                {
                    Console.Write("Removed cap.");
                    Console.WriteLine();
                }
                else
                {
                    Console.Write("ERROR removing cap:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
                 */

                return res.StatusCode.ToString();
            }
        }


        //CAPABILITY TYPE MANAGEMENT ******************************************************

        //GET
        public string getCapabilityTypes()
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);


                return client.GetAsync("capability-types")
                    .Result
                    .Content.ReadAsStringAsync().Result;
            }

        }

        //GET
        public string getCapabilityType(int id)
        {
            string capabilityType = String.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.GetAsync("capability-types/" + id);
                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    using (HttpContent content = res.Content)
                    {
                        capabilityType = content.ReadAsStringAsync().Result;
                    }
                }
                else
                {
                    Console.Write("ERROR:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return capabilityType;
        }

        //POST does not work. Use PUT instead.
        public string addCapabilityType(CapabilityType capabilityType)
        {

            string newCapabilityType = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<capabilityType xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<name>" + capabilityType.Name + "</name>" +
                    "<requestMessageType>" + capabilityType.RequestMessageType + "</requestMessageType>" +
                    "<responseMessageType>" + capabilityType.ResponseMessageType + "</responseMessageType>" +
                    "<schemaLocation>" + capabilityType.SchemaLocation + "</schemaLocation>" +
                    "<contextPath>" + capabilityType.ContextPath + "</contextPath>" +
                    "</capabilityType>";

                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                //Task<HttpResponseMessage> response = client.PostAsync("capability-types", capabilityType);
                Task<HttpResponseMessage> response = client.PutAsync("capability-types", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newCapabilityType = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR adding capType:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newCapabilityType;
        }

        //PUT
        public string setCapabilityType(CapabilityType capabilityType)
        {

            string newCapabilityType = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<capabilityType xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<name>" + capabilityType.Name + "</name>" +
                    "<requestMessageType>" + capabilityType.RequestMessageType + "</requestMessageType>" +
                    "<responseMessageType>" + capabilityType.ResponseMessageType + "</responseMessageType>" +
                    "<schemaLocation>" + capabilityType.SchemaLocation + "</schemaLocation>" +
                    "<contextPath>" + capabilityType.ContextPath + "</contextPath>" +
                    "</capabilityType>";

                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                Task<HttpResponseMessage> response = client.PutAsync("capability-types", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newCapabilityType = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR updating capType:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newCapabilityType;
        }

        //DELETE
        public string removeCapabilityType(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.DeleteAsync("capability-types/" + id);
                HttpResponseMessage res = response.Result;

                /*
                if (res.IsSuccessStatusCode)
                {
                    Console.Write("Removed capType.");
                    Console.WriteLine();
                }
                else
                {
                    Console.Write("ERROR removing capType:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
                 */

                return res.StatusCode.ToString();
            }
        }


        //BUSINESS PROCESS TEMPLATE MANAGEMENT ******************************************************

        //GET
        public string getBusinessProcessTemplates()
        {

            //string businessProcessTemplates = String.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);


                return client.GetAsync("business-process-templates")
                    .Result
                    .Content.ReadAsStringAsync().Result;
            }

        }

        //GET
        public string getBusinessProcessTemplate(int id)
        {
            string businessProcessTemplate = String.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.GetAsync("business-process-templates/" + id);
                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    using (HttpContent content = res.Content)
                    {
                        businessProcessTemplate = content.ReadAsStringAsync().Result;
                    }
                }
                else
                {
                    Console.Write("ERROR:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return businessProcessTemplate;
        }

        //POST does not work. Use PUT instead.
        public string addBusinessProcessTemplate(BusinessProcessTemplate businessProcessTemplate)
        {

            string newBusinessProcessTemplate = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);
                
                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<BusinessProcessTemplate xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<name>" + businessProcessTemplate.Name + "</name>";
                                    
                List<int> ctList = businessProcessTemplate.CapabilityTypes;
                if (ctList != null) {

                    foreach (int capType in ctList) // Display for verification.
                    {
                        myMessage = myMessage + "<capabilityTypes>"+ "<id>" + capType + "</id>" + 
                                                "<name>name</name>" + 
                                                "<requestMessageType>request</requestMessageType>" +
                                                "<responseMessageType>response</responseMessageType>" +
                                                "<schemaLocation>schema</schemaLocation>" +
                                                "<contextPath>contexPath</contextPath>" +
                                                "</capabilityTypes>";
                    }
                    
                }

                List<BusinessProcessTemplate.BusinessProcessTemplateRole> bptRoleList = businessProcessTemplate.Role;
                if (bptRoleList != null)
                {

                    foreach (BusinessProcessTemplate.BusinessProcessTemplateRole bptRole in bptRoleList) // Display for verification.
                    {
                        myMessage = myMessage + "<role>" + "<id>" + bptRole.RoleName + "</id>";

                        foreach (int capType in bptRole.CapabilityTypes) // Display for verification.
                        {
                            myMessage = myMessage + "<capabilityType>" + "<id>" + capType + "</id>" +
                                                    "<name>name</name>" +
                                                    "<requestMessageType>request</requestMessageType>" +
                                                    "<responseMessageType>response</responseMessageType>" +
                                                    "<schemaLocation>schema</schemaLocation>" +
                                                    "<contextPath>contexPath</contextPath>" +
                                                    "</capabilityType>";
                        }

                        myMessage = myMessage + "</role>";
                    }

                }


                myMessage = myMessage + "</BusinessProcessTemplate>";

                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                //Task<HttpResponseMessage> response = client.PostAsync("business-process-templates", businessProcessTemplate);
                Task<HttpResponseMessage> response = client.PutAsync("business-process-templates", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newBusinessProcessTemplate = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR adding bpt:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newBusinessProcessTemplate;
        }

        //PUT
        public string setBusinessProcessTemplate(BusinessProcessTemplate businessProcessTemplate)
        {
            string newBusinessProcessTemplate = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<BusinessProcessTemplate xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<name>" + businessProcessTemplate.Name + "</name>";

               List<int> ctList = businessProcessTemplate.CapabilityTypes;
                if (ctList != null)
                {

                    foreach (int capType in ctList) // Display for verification.
                    {
                        myMessage = myMessage + "<capabilityTypes>" + "<id>" + capType + "</id>" +
                                                "<name>name</name>" +
                                                "<requestMessageType>request</requestMessageType>" +
                                                "<responseMessageType>response</responseMessageType>" +
                                                "<schemaLocation>schema</schemaLocation>" +
                                                "<contextPath>contexPath</contextPath>" +
                                                "</capabilityTypes>";
                    }

                }

                List<BusinessProcessTemplate.BusinessProcessTemplateRole> bptRoleList = businessProcessTemplate.Role;
                if (bptRoleList != null)
                {

                    foreach (BusinessProcessTemplate.BusinessProcessTemplateRole bptRole in bptRoleList) // Display for verification.
                    {
                        myMessage = myMessage + "<role>" + "<id>" + bptRole.RoleName + "</id>";

                        foreach (int capType in bptRole.CapabilityTypes) // Display for verification.
                        {
                            myMessage = myMessage + "<capabilityType>" + "<id>" + capType + "</id>" +
                                                    "<name>name</name>" +
                                                    "<requestMessageType>request</requestMessageType>" +
                                                    "<responseMessageType>response</responseMessageType>" +
                                                    "<schemaLocation>schema</schemaLocation>" +
                                                    "<contextPath>contexPath</contextPath>" +
                                                    "</capabilityType>";
                        }

                        myMessage = myMessage + "</role>";
                    }

                }


                myMessage = myMessage + "</BusinessProcessTemplate>";
                
                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                Task<HttpResponseMessage> response = client.PutAsync("business-process-templates", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newBusinessProcessTemplate = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR updating bpt:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newBusinessProcessTemplate;
        }

        //DELETE
        public string removeBusinessProcessTemplate(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.DeleteAsync("business-process-templates/" + id);
                HttpResponseMessage res = response.Result;

                /*
                if (res.IsSuccessStatusCode)
                {
                    Console.Write("Removed BusinessProcessTemplate.");
                    Console.WriteLine();
                }
                else
                {
                    Console.Write("ERROR removing BusinessProcessTemplate:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
                 */

                return res.StatusCode.ToString();
            }
        }


        //BUSINESS PROCESS  MANAGEMENT ******************************************************

        //GET
        public string getBusinessProcesses()
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);


                return client.GetAsync("business-processes")
                    .Result
                    .Content.ReadAsStringAsync().Result;
            }

        }

        //GET
        public string getBusinessProcess(int id)
        {
            string businessProcess = String.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.GetAsync("business-processes/" + id);
                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    using (HttpContent content = res.Content)
                    {
                        businessProcess = content.ReadAsStringAsync().Result;
                    }
                }
                else
                {
                    Console.Write("ERROR:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return businessProcess;
        }

        //POST does not work. Use PUT instead.
        public string addBusinessProcess(BusinessProcess businessProcess)
        {

            string newBusinessProcess = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<BusinessProcess xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<name>" + businessProcess.Name + "</name>" +
                    "<bpt_id>" + businessProcess.BptId + "</bpt_id>";

                List<int> cList = businessProcess.Capabilities;
                if (cList != null)
                {
                    foreach (int cap in cList) // Display for verification.
                    {
                        string c = this.getCapability (cap);
                        
                        XmlDocument xml = new XmlDocument();
                        xml.LoadXml(c);

                        XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
                        nsmgr.AddNamespace("ns2", "http://www.limetri.eu/schemas/ygg");

                        XmlNodeList xnList = xml.SelectNodes("/ns2:Capability", nsmgr);

                        string capType = "";
                        foreach (XmlNode xn in xnList)
                        {
                            capType = xn["ns2:cte_id"].InnerText;

                            //Console.WriteLine("cte_id: " + capType);
                        }
                        myMessage = myMessage + "<capabilities>" + "<id>" + cap + "</id>" +
                                    "<description>desc</description>"+
                                    "<uri>uri</uri>" +
                                    "<cte_id>" + capType + "</cte_id>" + "</capabilities>";
                    }
                    
                }


                List<BusinessProcess.BusinessProcessRole> bpRoleList = businessProcess.Role;
                if (bpRoleList != null)
                {
                    foreach (BusinessProcess.BusinessProcessRole bpRole in bpRoleList) 
                    {
                        myMessage = myMessage + "<role>" + "<id>" + bpRole.RoleName + "</id>";

                        foreach (int cap in bpRole.Capability) 
                        {
                            string c = this.getCapability(cap);

                            XmlDocument xml = new XmlDocument();
                            xml.LoadXml(c);

                            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
                            nsmgr.AddNamespace("ns2", "http://www.limetri.eu/schemas/ygg");

                            XmlNodeList xnList = xml.SelectNodes("/ns2:Capability", nsmgr);

                            string capType = "";
                            foreach (XmlNode xn in xnList)
                            {
                                capType = xn["ns2:cte_id"].InnerText;

                                //Console.WriteLine("cte_id: " + capType);
                            }
                            myMessage = myMessage + "<capability>" + "<id>" + cap + "</id>" +
                                        "<description>desc</description>" +
                                        "<uri>uri</uri>" +
                                        "<cte_id>" + capType + "</cte_id>" + "</capability>";
                        }

                        myMessage = myMessage + "</role>";
                    }

                }
                myMessage = myMessage + "</BusinessProcess>";
                
                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                //Task<HttpResponseMessage> response = client.PostAsync("business-processes", businessProcess);
                Task<HttpResponseMessage> response = client.PutAsync("business-processes", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newBusinessProcess = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR adding bp:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newBusinessProcess;
        }

        //PUT
        public string setBusinessProcess(BusinessProcess businessProcess)
        {
            string newBusinessProcess = String.Empty;
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                String myMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<BusinessProcess xmlns=\"http://www.limetri.eu/schemas/ygg\">" +
                    "<name>" + businessProcess.Name + "</name>" +
                    "<bpt_id>" + businessProcess.BptId + "</bpt_id>";

                List<int> cList = businessProcess.Capabilities;
                if (cList != null)
                {
                    foreach (int cap in cList) // Display for verification.
                    {
                        string c = this.getCapability(cap);

                        XmlDocument xml = new XmlDocument();
                        xml.LoadXml(c);

                        XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
                        nsmgr.AddNamespace("ns2", "http://www.limetri.eu/schemas/ygg");

                        XmlNodeList xnList = xml.SelectNodes("/ns2:Capability", nsmgr);

                        string capType = "";
                        foreach (XmlNode xn in xnList)
                        {
                            capType = xn["ns2:cte_id"].InnerText;

                            //Console.WriteLine("cte_id: " + capType);
                        }
                        myMessage = myMessage + "<capabilities>" + "<id>" + cap + "</id>" +
                                    "<description>desc</description>" +
                                    "<uri>uri</uri>" +
                                    "<cte_id>" + capType + "</cte_id>" + "</capabilities>";
                    }

                }
                myMessage = myMessage + "</BusinessProcess>";

                HttpContent content = new StringContent(myMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                Task<HttpResponseMessage> response = client.PutAsync("business-processes", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    newBusinessProcess = res.Content.ReadAsStringAsync().Result;

                }
                else
                {
                    Console.Write("ERROR updating bp:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
            }
            return newBusinessProcess;
        }

        //DELETE
        public string removeBusinessProcess(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(constants.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                Task<HttpResponseMessage> response = client.DeleteAsync("business-processes/" + id);
                HttpResponseMessage res = response.Result;

                /*
                if (res.IsSuccessStatusCode)
                {
                    Console.Write("Removed BusinessProcess.");
                    Console.WriteLine();
                }
                else
                {
                    Console.Write("ERROR removing BusinessProcess:" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
                 */

                return res.StatusCode.ToString();
            }
        }



        //USE
        public string useCapability(Capability capability, string requestMessage)
        {
            string capUsed = String.Empty;
            string capTypeIdentifier = String.Empty;


            string capType = this.getCapabilityType(capability.CteId);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(capType);
            var nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ns2", "http://www.limetri.eu/schemas/ygg");

            XmlNode node = doc.DocumentElement.FirstChild;
            XmlNode node1 = node.SelectSingleNode("//ns2:identifier", nsmgr);
            capTypeIdentifier = node1.InnerText;

            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri(constants.Url_Use);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                // Adding access token
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.AccessToken);

                HttpContent content = new StringContent(requestMessage);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/xml");
                Task<HttpResponseMessage> response = client.PostAsync("capabilities/" + capTypeIdentifier + "/use", content);

                HttpResponseMessage res = response.Result;
                if (res.IsSuccessStatusCode)
                {
                    Console.Write("Capability " +  capability.Description + " used successfully!");
                    Console.WriteLine();
                    capUsed = res.StatusCode.ToString();

                }
                else
                {
                    Console.Write("ERROR using capability " + capability.Description + ":" + res.StatusCode + " " + res.ReasonPhrase);
                    Console.WriteLine();
                }
                 
            }
           return capUsed;
          
        }

    }
}
