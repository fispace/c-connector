﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FispaceClientApplication.Fispace.Api
{
    public class BusinessProcessTemplate
    {
         
        private int id;
        protected int Id
        {
            get { return id; }
            set { id = value; }
        }

        private String name;
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        private List<int> capabilityTypes;
        public List<int> CapabilityTypes
        {
            get { return capabilityTypes; }
            set { capabilityTypes = value; }
        }

        public class BusinessProcessTemplateRole
        {
            private String roleName;
            public String RoleName
            {
                get { return roleName; }
                set { roleName = value; }
            }

            private List<int> capabilityTypes;
            public List<int> CapabilityTypes
            {
                get { return capabilityTypes; }
                set { capabilityTypes = value; }
            }

            public BusinessProcessTemplateRole(string myRoleName, List<int> myCapTypes)
            {
                roleName = myRoleName;
                capabilityTypes = new List<int>();

                if (myCapTypes != null)
                {
                    foreach (int capTypeAux in myCapTypes) // Display for verification.
                    {
                        capabilityTypes.Add(capTypeAux);
                    }
                }
            }
        }  

        private List<BusinessProcessTemplateRole> role;
        public List<BusinessProcessTemplateRole> Role
        {
            get { return role; }
            set { role = value; }
        }

        /*
        private int capType;
        public int CapType
        {
            get { return capType; }
            set { capType = value; }
        }
        */

        public BusinessProcessTemplate(string myName, List<int> myCapTypes)
        {
            name = myName;
            role = null;

            capabilityTypes = new List<int>();

            if (myCapTypes != null)
            {
                foreach (int capTypeAux in myCapTypes)
                {
                    capabilityTypes.Add(capTypeAux);
                }
            }           
        }

        public BusinessProcessTemplate(string myName, List<BusinessProcessTemplateRole> myBPTrole)
        {
            name = myName;
            role = new List<BusinessProcessTemplateRole>();

            capabilityTypes = null;

            if (myBPTrole != null)
            {
                foreach (BusinessProcessTemplateRole bptRoleAux in myBPTrole) 
                {
                    role.Add(bptRoleAux);
                }
            }
        }
        
        public BusinessProcessTemplate(string myName, List<int> myCapTypes, List<BusinessProcessTemplateRole> myBPTrole)
        {
            name = myName;
            role = new List<BusinessProcessTemplateRole>();

            capabilityTypes = new List<int>();

            if (myCapTypes != null)
            {
                foreach (int capTypeAux in myCapTypes) 
                {
                    capabilityTypes.Add(capTypeAux);
                }
            }

            if (myBPTrole != null)
            {
                foreach (BusinessProcessTemplateRole bptRoleAux in myBPTrole) 
                {
                    role.Add(bptRoleAux);
                }
            }
        }
    }

}

