﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FispaceClientApplication.Fispace.Api
{
    class Capability
    {
        private int id;

        protected int Id
        {
            get { return id; }
            set { id = value; }
        }
        private String description;

        public String Description
        {
            get { return description; }
            set { description = value; }
        }
        private String uri;

        public String Uri
        {
            get { return uri; }
            set { uri = value; }
        }
        
        private int cteId;

        public int CteId
        {
            get { return cteId; }
            set { cteId = value; }
        }

        public Capability(string myDescription, string myUri, int CteId)
        {
            description = myDescription;
            uri = myUri;
            cteId = CteId;
            
        }
    }
}
