﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FispaceClientApplication.Fispace.Api
{
    public class BusinessProcessTemplateList
    {

        private BusinessProcessTemplate[] businessProcessTemplateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("businessProcessTemplate")]
        public BusinessProcessTemplate[] businessProcessTemplate
        {
            get
            {
                return this.businessProcessTemplateField;
            }
            set
            {
                this.businessProcessTemplateField = value;
            }
        }
    }
}