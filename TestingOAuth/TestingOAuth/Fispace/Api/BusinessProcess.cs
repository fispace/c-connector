﻿/*
 *  Version 0.16.0
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FispaceClientApplication.Fispace.Api
{
    class BusinessProcess
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private String name;
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        private int bptId;
        public int BptId
        {
            get { return bptId; }
            set { bptId = value; }
        }

        private List<int> capabilities;
        public List<int> Capabilities
        {
            get { return capabilities; }
            set { capabilities = value; }
        }

        public class BusinessProcessRole
        {
            private String roleName;
            public String RoleName
            {
                get { return roleName; }
                set { roleName = value; }
            }

            private List<int> capability;
            public List<int> Capability
            {
                get { return capability; }
                set { capability = value; }
            }

            public BusinessProcessRole(string myRoleName, List<int> myCaps)
            {
                roleName = myRoleName;
                capability = new List<int>();

                if (myCaps != null)
                {
                    foreach (int capAux in myCaps) 
                    {
                        capability.Add(capAux);
                    }
                }
            }
        }  

        private List<BusinessProcessRole> role;
        public List<BusinessProcessRole> Role
        {
            get { return role; }
            set { role = value; }
        }

        public BusinessProcess(string myName, int myBpt, List<int> myCaps)
        {
            name = myName;
            bptId = myBpt;
            role = null;

            capabilities = new List<int>();

            if (myCaps != null)
            {
                foreach (int capAux in myCaps)
                {
                    capabilities.Add(capAux);
                }
            }           
        }

        public BusinessProcess(string myName, int myBpt, List<BusinessProcessRole> myBProle)
        {
            name = myName;
            bptId = myBpt;
            role = new List<BusinessProcessRole>();

            capabilities = null;

            if (myBProle != null)
            {
                foreach (BusinessProcessRole bpRoleAux in myBProle) 
                {
                    role.Add(bpRoleAux);
                }
            }
        }

        public BusinessProcess(string myName, int myBpt, List<int> myCaps, List<BusinessProcessRole> myBProle)
        {
            name = myName;
            bptId = myBpt;
            capabilities = new List<int>();
            role = new List<BusinessProcessRole>();

            if (myCaps != null) {
                foreach (int capTypeAux in myCaps) 
                {
                    capabilities.Add(capTypeAux);
                }
            }

            if (myBProle != null)
            {
                foreach (BusinessProcessRole bpRoleAux in myBProle)
                {
                    role.Add(bpRoleAux);
                }
            }
            
        }
    }
}
