﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp.Contrib;

using TestingOAuth.OAuth2.Framework;
using TestingOAuth.OAuth2;

namespace TestingOAuth.OAuth2.Providers
{
    /// <summary>
    /// OAuth 2 provider for Keycloak.
    /// </summary>
    public class KeycloakOAuth : OAuth2ProviderDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeycloakOAuth"/> class.
        /// </summary>
        public KeycloakOAuth()
        {
            //LoginEndpointUri = "http://37.131.251.159:8080/auth/realms/fispace/tokens/login";
            AuthorizationEndpointUri = "http://37.131.251.159:8080/auth/realms/fispace/tokens/grants/access"; //?
            TokenEndpointUri = "http://37.131.251.159:8080/auth/realms/fispace/tokens/access/codes";
            // LogoutEndpointUri = "http://37.131.251.159:8080/auth/realms/fispace/tokens/logout";
        }


        // VER GoogleOAuth2:
        
        /// <summary>
        /// Returns a list of parameters to be included in authorization endpoint as query string. 
        /// </summary>
        /// <param name="appCredentials">The user's application credentials.</param>
        /// <param name="redirectUri">The redirect URI in which OAuth user wishes sites user to be returned to finally</param>
        /// <param name="scope">The scope of access or set of permissions OAuth user is demanding.</param>
        /// <param name="stateManager">An implementation of <see cref="IOAuth20StateManager"/> for providing state value.</param>
        /// <returns>A list of parameters to be included in authorization endpoint.</returns>
        public override ParameterSet GetAuthorizationRequestParameters(ApplicationCredentials appCredentials, string redirectUri, string scope, IOAuth20StateManager stateManager)
        {
            return new ParameterSet(new Dictionary<string, string> {
                {"client_id", appCredentials.AppId},
                {"redirect_uri", redirectUri},
                {"scope", scope},
                {"state", stateManager != null ? stateManager.GetState() : null},
                {"response_type", "code"},
                //{"access_type", offline ? "offline" : "online"}, // VER GoogleOAuth2
                //{"approval_prompt", forceApprovalPrompt ? "force" : "auto"},
            });
        }



        //EN KEYCLOAK: refresh token
        // public ProcessUserResponseOutput(ParameterSet allParameters, string accessToken, DateTime? expires = null, string refreshToken = null)

        // VER FacebookOAuth2:

        /// <summary>
        /// Parses the response body to request to access token.
        /// </summary>
        /// <param name="body">The response body of request for access token.</param>
        /// <returns>A <see cref="ProcessUserResponseOutput"/> object containing access token and some additional parameters or error details.</returns>
        public override ProcessUserResponseOutput ParseAccessTokenResult(string body)
        {
            var values = HttpUtility.ParseQueryString(body);
            var allParameters = new ParameterSet();
            foreach (var v in values.Keys.Cast<string>()) allParameters.Add(v, values[v]);
            var accessToken = values["access_token"];
            DateTime? expires = null;
            if (null != values["expires"])
                expires = DateTime.Now.AddSeconds(int.Parse(values["expires"]));
            return new ProcessUserResponseOutput(allParameters, accessToken, expires);
        }


        // VER GithubOAuth2:

        // <summary>
        /// Parses the response body to request to access token.
        /// </summary>
        /// <param name="body">The response body of request for access token.</param>
        /// <returns>A <see cref="ProcessUserResponseOutput"/> object containing access token and some additional parameters or error details.</returns>
        /*
        public override ProcessUserResponseOutput ParseAccessTokenResult(string body)
        {
            var values = HttpUtility.ParseQueryString(body);
            var allParameters = new ParameterSet();
            foreach (var v in values.Keys.Cast<string>()) allParameters.Add(v, values[v]);
            var accessToken = values["access_token"];
            return new ProcessUserResponseOutput(allParameters, accessToken);
        }
        */
    }
          
    
}
