﻿using System;
using System.Text;

namespace TestingOAuth.OAuth2.Framework
{
    /// <summary>
    /// Methods to be used to handle passing parameters as query string.
    /// </summary>
    internal static class ParametersUtil
    {
        /// <summary>
        /// Given a list of parameters, converts them to a URI query string.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A URI query string containing entries for each parameter.</returns>
        public static string AsQueryString(ParameterSet parameters)
        {
            var queryBuilder = new StringBuilder(parameters.Get().Length * 20);
            foreach (var param in parameters.Get()) {
                queryBuilder.Append(param.Name);
                queryBuilder.Append('=');
                queryBuilder.Append(param.Value);
                queryBuilder.Append('&');
            }
            var query = queryBuilder.ToString(0, queryBuilder.Length - 1);
            return query;
        }
        /// <summary>
        /// Creates the URI with the specified parameters.
        /// </summary>
        /// <param name="endpoint">The endpoint URI.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>A complete URI with parameters appended as query string.</returns>
        public static Uri CreateUri(string endpoint, ParameterSet parameters)
        {
            var uriBuilder = new UriBuilder(endpoint) {Query = AsQueryString(parameters)};
            return uriBuilder.Uri;
        }
    }

}