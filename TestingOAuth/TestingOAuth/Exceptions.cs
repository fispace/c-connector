﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingOAuth
{
    public class InvalidOAuthStateException : Exception
    {
        public InvalidOAuthStateException()
            : base("Invalid state or state not recognized.")
        {
        }
    }
}
